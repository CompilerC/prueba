#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define TAM 10

void aleatorio(int d[TAM][TAM],int num);
void formateadatos(int d[TAM][TAM]);
void formateatablero(int d[TAM][TAM],int jugada[TAM][TAM]);
void asigna(int d[TAM][TAM],int num);

void solucion(int d[TAM][TAM]);
void tablero(int jugada[TAM][TAM]);

void main()
{
	srand(time(NULL));
	int datos[TAM][TAM];
	int jugadas[TAM][TAM];
	int num;
	
	printf("Por favor introduzca el numero de parejas: ");
	fflush(stdin);
	scanf("%d",&num);
	num=num*2;
	
	formateadatos(datos);
	aleatorio(datos,num);
	asigna(datos,num);
	formateatablero(datos,jugadas);
	
	solucion(datos);
	
	tablero(jugadas);

	
getch();	
}



void formateadatos(int d[TAM][TAM])
{
	
	int i,j;
	
	for (i=0;i<TAM;i++)
	{
		for(j=0;j<TAM;j++)
		{
			d[i][j]=-1;
					
		}		
	}

}

void formateatablero(int d[TAM][TAM],int jugada[TAM][TAM])
{
	int i,j;
	
	for (i=0;i<TAM;i++)
	{
		for(j=0;j<TAM;j++)
		{	
			if(d[i][j]!=0)
			jugada[i][j]='#';
			else
			jugada[i][j]='X';
					
		}		
	}
}

void aleatorio(int d[TAM][TAM],int num)
{
	int i,j;
	int aux;
	int cont;
	int fin=0;
	

	do
	{	
		cont=0;
		aux=rand()%num;
		for(i=0;i<TAM;i++)
		{
			for(j=0;j<TAM;j++)
			{			
			if(aux==d[i][j]) cont++;
			}
		}
		
		for(i=0;i<TAM && fin!=num;i++)
		{
			for(j=0;j<TAM && fin!=num;j++)
			{
				if(cont==0 && d[i][j]==-1) 
				{
				d[i][j]=aux;
				cont++;
				fin++;
				}
				//if(d[i][j]==-1)fin++;
			}
		}

	}while(fin!=num);
}	
	
void asigna(int d[TAM][TAM],int num)
{
	int i,j;
	

		for(i=0;i<TAM;i++)
		{
			for(j=0;j<TAM;j++)
			{			
			d[i][j]=d[i][j]+1;
			if(d[i][j]>num/2) d[i][j]=d[i][j]-num/2;
			}
		}
	
}	

void solucion(int d[TAM][TAM])
{
	int i,j;
	
	printf("\n");
	printf("     0     1     2     3     4     5     6     7     8     9 \n");
	printf("   ____________________________________________________________");
	printf("\n");
	for (i=0;i<10;i++)
	{
		for(j=0;j<11;j++)
		{
			if(j<10)
			{	
					
				if(j==0)printf("%d ",i);
				if(d[i][j]!=0)
				{		
				if(d[i][j]<10)
				printf("|  %d  ",d[i][j]);
				else if(d[i][j]<100)
				printf("| %d  ",d[i][j]);
				else 
				printf("| %d ",d[i][j]);
				}	
				else
				printf("|  X  ");
			}
			
			else
			printf("|");
		}
	printf("\n");	
	printf("  -------------------------------------------------------------");
	printf("\n");	
	
	}
	printf("\n");

printf("Presione cualquier letra para ocultar el tablero\n\n");
getch();
system("cls");
	
}
void tablero(int jugada[TAM][TAM])
{
	int i,j;
	
	printf("\t\t\tTablero de Jugadas\n");
	
	printf("\n");
	printf("     0     1     2     3     4     5     6     7     8     9 \n");
	printf("   ____________________________________________________________");
	printf("\n");
	
	for (i=0;i<10;i++)
	{
		for(j=0;j<11;j++)
		{
			if(j<10)
			{
				if(j==0) printf("%d ",i);
			printf("|  %c  ",jugada[i][j]);
			}
			
			else
			printf("|");
		}
	printf("\n");	
	printf("  -------------------------------------------------------------");
	printf("\n");	
	
	}
	printf("\n");
}

